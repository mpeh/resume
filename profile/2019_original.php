<li>Full stack developer with experience architecting complex data management systems while enhancing the user's web experience</li>
<li>Over six years experience developing with Symfony PHP framework</li>
<li>Proven problem-solving and troubleshooting skills</li>
<li>Resourceful and goal-oriented with strong ability to work alone or within a team</li>
<li>Comfortable in making decisions and facing new challenges</li>
