<ul class="experience_list">
  <li>Developed from ground up, including the roster processing system, Lexile placement test, reporting system</li>
  <li>Pioneered the initial development of two Java Spring Boot-based services to improve data processing performance and to encapsulate reusable core business logic.</li>
  <li>Developed an in-house dashboard operating as a switchboard for controlling and directing code delivery from staging environment to production.</li>
  <li>Operated in a DevOps role and instrumental in introducing continuous integration (Gitlab CI and Docker) to help facilitate continuous delivery of code artifacts.</li>
</ul>

- Beable (litaracy product) TestAhead (test-prep product)