This repository contains PHP scripts that I use to generate my resume according to different needs.

It uses the [mPDF library](https://github.com/mpdf/mpdf) to generate a PDF.